#!/bin/env bash

# Sets up and mounts the E3 NFS share.
#
# This will allow a more standard, simple way for integrators and developers to mount the correct E3 version.

declare -r E3_NFS_HOST="e3-share-lab.cslab.esss.lu.se"
declare -r REMOTE_MOUNT_POINT="$E3_NFS_HOST:/e3"
declare -r SCRIPT_NAME="$0"
declare -r DEFAULT_BASE="7.0.4"
declare -r DEFAULT_REQ="3.2.0"

function die() {
    if [ $# -ge 1 ]; then
        echo "Error: $1" >&2
    else
        echo "Error: $SCRIPT_NAME failed." >&2
    fi
    exit 1
}

function usage() {
    echo "Setups up and mounts the E3 NFS share"
    echo ""
    echo "Usage: bash setupNFS.sh -d DIR [-u] [-b BASE] [-r REQ] [-h]"
    echo "  -d: Mount/unmmount NFS E3 in directory \$DIR"
    echo "  -u: Unmounts the directory \$DIR"
    echo "  -b: Select EPICS base version \$BASE"
    echo "  -r: Select require version \$REQ"
    echo "  -h: Show this message "
}

showmount -e $E3_NFS_HOST | grep "/e3\s" &> /dev/null || die "E3 NFS share is not mountable. Please check that you are on the ESS VPN."

BASE="$DEFAULT_BASE"
REQ="$DEFAULT_REQ"
while getopts "d:b:r:uh" opt; do
    case $opt in
        d) DIR="$OPTARG"
            ;;
        b) BASE="$OPTARG"
            ;;
        r) REQ="$OPTARG"
            ;;
        u) UNMOUNT=true
            ;;
        h) help=true
            ;;
    esac
done

if [ "$help" = true ]; then
    usage
    exit 0
fi

if [ "$DIR" = "" ]; then
    usage
    die "You must specify a directory name"
fi

if [ "$UNMOUNT" = true ]; then
    echo "Attempting to unmount $DIR..."
    sudo umount "$DIR" || die "Failed to unmount NFS directory"
    exit 0
fi

if [ ! -d "$DIR" ]; then
    echo "Directory '$DIR' does not exist. Attempting to create it..."
    if ! mkdir -p "$DIR"; then
        echo "Could not create '$DIR'. Retrying with sudo..." >&2
        sudo mkdir -p "$DIR" || die "Could not create '$DIR'"
    fi
fi

if mount | awk '{ print $1 " " $3 }' | grep "$REMOTE_MOUNT_POINT `realpath $DIR`"; then
    echo "Directory $DIR already mounted"
else
    sudo mount -r "$REMOTE_MOUNT_POINT" "$DIR" || die "Failed to mount NFS directory"
fi

if [ ! -d "$DIR/base-$BASE" ]; then
    echo "EPICS base verion $BASE does not exist at selected mount point" >&2
    echo "Valid versions are:" >&2
    for d in $DIR/base-*; do
        echo `basename $d`
    done
    exit 1
fi

if [ ! -d "$DIR/base-$BASE/require/$REQ" ]; then
    echo "Require version $REQ does not exist for EPICS base $BASE in mounted directory" >&2
    echo "Valid versions are:" >&2
    for d in $DIR/base-$BASE/require/*; do
        echo `basename $d`
    done
    exit 1
fi 

source "$DIR/base-$BASE/require/$REQ/bin/setE3Env.bash"