#!/bin/bash

# Description:
# Script to clone a group of modules
# Author: Wayne Lewis
# Date: 2020-07-15

# Options:
# -f: input inventory file
# -g: group to clone
# -v: verbose
# -s: use https instead of ssh to clone
# -h: help

# Process options
while getopts "f:g:vsh" opt; do
  case $opt in
    f) input_file="$OPTARG"
      ;;
    g) group="$OPTARG"
      ;;	
    v) verbose=true
      ;;
    s) ssh=false
      ;;
    h) help=true
      ;;
    \?) echo "Invalid option -$OPTARG" >&2
      ;;
  esac
done

if [ "$help" = true ]; then
  echo "Clone all modules whose path matches a defined gitlab subgroup name."
  echo ""
  echo "usage: ./clone-group-modules.sh -f <path/to/inventory/file> -g <group_name> [-v] [-h]"
  echo "parameters:"
  echo "-f: specify inventory file path"
  echo "-g: specify group to clone"
  echo "-s: use https instead of ssh to clone"
  echo "-v: be verbose"
  echo "-h: print this usage information"
  exit 0
fi

clone_modules() {
  pwd=`pwd`
  [[ "$verbose" = true ]] && echo "$pwd"

  # Iterate through the inventory file. # lines are ignored as comments.
  for dir in `cat "$input_file" | grep e3 | grep -v "#" | grep "$group"`; do
    [[ "$verbose" = true ]] && echo "$dir"
    if [[ "$ssh" = false ]]; then
      git clone --origin ess-https https://gitlab.esss.lu.se/e3/"$dir".git
    else
      git clone --origin ess-ssh git@gitlab.esss.lu.se:e3/$dir	
    fi
  done
}

clone_modules
