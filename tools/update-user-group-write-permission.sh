#!/bin/bash

# Description:
# Script to manage user and group write permission on files 
# Author: Wayne Lewis
# Date: 2020-07-31

# Options:
# -a: add user/group write permission
# -r: remove user/group write permission
# -v: verbose
# -h: help

# Process options
while getopts "arvh" opt; do
  case $opt in
    a) add_write_permission=true
      ;;
    r) remove_write_permission=true
      ;;
    v) verbose=true
      ;;
    h) help=true
      ;;
    \?) echo "Invalid option -$OPTARG" >&2
      ;;
  esac
done

if [ "$help" = true ]; then
  echo "Manage user and group writeable permissions on all files"
  echo "owned by current user in or under the current directory."
  echo "usage: ./update-user-group-write-permission [-a|-r] [-v] [-h]"
  echo "parameters:"
  echo "-a: add user and group write permission"
  echo "-r: remove user and group write permission"
  echo "-v: be verbose"
  echo "-h: print this usage information"
  exit 0
fi

update_file_permission() {
  whoami=`whoami`
  # Check that we are in the right part of the directory tree. We want
  # to make sure we are somewhere under the siteMods tree structure when
  # we run this script.
  if [[ $PWD = */siteMods* ]]; then
    if [ "$verbose" = true ]; then
      echo "Changing user and group write permissions on all files owned by $whoami"
      find . -user "$whoami" -type f
    fi
    if [ "$add_write_permission" = true ]; then
      find . -user "$whoami" -type f -exec chmod ug+w {} \; > /dev/null
    elif [ "$remove_write_permission" =  true ]; then
      find . -user "$whoami" -type f -exec chmod ug-w {} \; > /dev/null
    else
      echo "Neither -a nor -r specified. Doing nothing"
    fi
  else
    echo "Cannot find siteMods in current path. This should be run at or below the siteMods directory."
    exit -1
  fi
}

update_file_permission
