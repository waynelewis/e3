#!/bin/bash

# Description:
# Script to build all module groups
# Author: Wayne Lewis
# Date: 2020-07-15

# Options:
# -f: input inventory file
# -e: abort on error flag
# -i: install flag
# -v: verbose
# -h: help

# Process options
while getopts "f:eivh" opt; do
  case $opt in
    f) input_file="$OPTARG"
      ;;
    e) abort_on_error=true
      ;;
    i) install=true
      ;;
    v) verbose=true
      ;;
    h) help=true
      ;;
    \?) echo "Invalid option -$OPTARG" >&2
      ;;
  esac
done

if [ "$help" = true ]; then
  echo "Build all modules whose path matches a defined gitlab subgroup name."
  echo ""
  echo "usage: ./build-all-modules.sh -f <path/to/inventory/file> [-e] [-i] [-v] [-h]"
  echo "parameters:"
  echo "-f: specify inventory file path"
  echo "-e: abort on build errors"
  echo "-i: install the built module"
  echo "-v: be verbose"
  echo "-h: print this usage information"
  exit 0
fi

build_modules() {

  script=`realpath $0`
  script_path=`dirname "$script"`

  [[ "$install" = true ]] && install_flag=-i
  [[ "$verbose" = true ]] && verbose_flag=-v
  [[ "$abort_on_error" = true ]] && abort_on_error_flag=-e
 
  group_names=(common timing ecat ifc ps llrf vac area psi)
  for group_name in ${group_names[*]}; do
    [ "$verbose" = true ] && echo "Group = $group_name"
    . "$script_path/build-group-modules.sh" "-f $input_file -g $group_name $install_flag $verbose_flag $abort_on_error_flag"
  done

}

build_modules
