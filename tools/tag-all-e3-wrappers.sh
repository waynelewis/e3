#!/bin/bash

# Description:
# Script to tag all e3 module wrappers
# Author: Wayne Lewis
# Date: 2020-07-15

# Options:
# -f: input inventory file
# -i: install flag
# -v: verbose
# -h: help

# Process options
while getopts "f:tpvh" opt; do
  case $opt in
    f) input_file="$OPTARG"
      ;;
    t) tag_repo=true
      ;;
    p) push=true
      ;;
    v) verbose=true
      ;;
    h) help=true
      ;;
    \?) echo "Invalid option -$OPTARG" >&2
      ;;
  esac
done

if [ "$help" = true ]; then
  echo "Tag all e3 wrapper modules based on the inventory file"
  echo ""
  echo "usage: ./tag-all-e3-wrappers -f <path/to/inventory/file> [-t] [-p] [-v] [-h]"
  echo "parameters:"
  echo "-f: specify inventory file path"
  echo "-t: tag repositories"
  echo "-p: push to remote"
  echo "-v: be verbose"
  echo "-h: print this usage information"
  exit 0
fi

tag_wrappers() {

  script=`realpath $0`
  script_path=`dirname "$script"`

  pwd=`pwd`
  [[ "$verbose" = true ]] && echo "$pwd"

  [[ "$tag_repo" = true ]] && tag_repo_flag=-t
  [[ "$push" = true ]] && push_flag=-p
  [[ "$verbose" = true ]] && verbose_flag=-v
 
  for dir in `cat "$input_file" | grep e3 | grep -v "#"`; do
    if [ -d "$dir" ]; then
      cd "$dir"
      . "$script_path"/tag-e3-wrapper.sh "$tag_repo_flag" "$push_flag" "$verbose_flag"
      cd "$pwd"
    else
      echo "Could not find $dir"
    fi
  done
}

tag_wrappers
