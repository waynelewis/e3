#!/bin/bash

# Description:
# Script to build a group of modules
# Author: Wayne Lewis
# Date: 2020-07-13

# Options:
# -f: input inventory file
# -e: abort on compile error
# -g: group to build
# -v: verbose
# -h: help

# Process options
while getopts "f:g:eivh" opt; do
  case $opt in
    f) input_file="$OPTARG"
      ;;
    g) group_name="$OPTARG"
      ;;	
    e) abort_on_error=true
      ;;
    i) install=true
      ;;
    v) verbose=true
      ;;
    h) help=true
      ;;
    \?) echo "Invalid option -$OPTARG" >&2
      ;;
  esac
done

if [ "$help" = true ]; then
  echo "Build all modules whose path matches a defined gitlab subgroup name."
  echo ""
  echo "usage: ./build-group-modules.sh -f <path/to/inventory/file> -g <group_name> [-v] [-h]"
  echo "parameters:"
  echo "-f: specify inventory file path"
  echo "-g: specify group to build"
  echo "-e: abort on compile error"
  echo "-i: install the built module"
  echo "-v: be verbose"
  echo "-h: print this usage information"
  exit 0
fi

echo "$group_name"
echo "$input_file"

build_modules() {
  pwd=`pwd`
  [[ "$verbose" = true ]] && echo "$pwd"

  # Iterate through the inventory file. # lines are ignored as comments.
  if [ ! -z "$group_name" ]; then
    for dir in `cat "$input_file" | grep e3 | grep -v "#" | grep "$group_name"/`; do
      [[ "$verbose" = true ]] && echo "Directory = $dir"
      [[ "$verbose" = true ]] && echo "Group = $group_name"
      if [ -d "$dir" ]; then
        cd "$dir"
        # Do a complete build of the modules
        make clean
        make init
        make patch
	[[ "$abort_on_error" = true ]] && set -e
        make build
	[[ "$abort_on_error" = true ]] && set +e
        [[ "$install" = true ]] && make install SUDO=
        cd "$pwd"
      else
        echo "Could not find $dir"
      fi
    done
  fi
}

build_modules
