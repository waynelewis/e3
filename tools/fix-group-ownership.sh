#!/bin/bash

# Description:
# Script to assign group ownership to e3rw_group
# Author: Wayne Lewis
# Date: 2020-07-13

# Options:
# -v: verbose
# -h: help

# Process options
while getopts "vh" opt; do
  case $opt in
    v) verbose=true
      ;;
    h) help=true
      ;;
    \?) echo "Invalid option -$OPTARG" >&2
      ;;
  esac
done

if [ "$help" = true ]; then
  echo "Set group ownership to e3rw_group for all files in or under the current"
  echo "directory and owned by the current user."
  echo "usage: ./fix-group-ownership.sh [-v] [-h]"
  echo "parameters:"
  echo "-v: be verbose"
  echo "-h: print this usage information"
fi

update_group_ownership() {
  whoami=`whoami`
  # Check that we are in the right part of the directory tree. We want
  # to make sure we are somewhere under the siteMods tree structure when
  # we run this script.
  if [[ $PWD = */siteMods* ]]; then
    if [ "$verbose" = true ]; then
      echo "Setting all files owned by $whoami to e3rw_group group ownership"
      find . -user "$whoami"
    fi
    find . -user "$whoami" -exec chgrp e3rw_group {} \; > /dev/null
  else
    echo "Cannot find siteMods in current path. This should be run at or below the siteMods directory."
    exit -1
  fi
}

update_group_ownership
